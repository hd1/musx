import logging
import unittest

import requests

import application

class MusixTests(unittest.TestCase):
    def testBadWord(self):
        words = application.LDNOOBW()
        logging.info(words)
        self.assertTrue('anal' in words)
        self.assertFalse('bitch' in words)

    def testAll(self):
        resp = requests.get('http://hasan.d8u.us:5001/').json()
        logging.error(resp)
        self.assertTrue(resp['response'] is not None)

    def testNew(self):
        user_id = 0
        resp = requests.post('http://hasan.d8u.us:5001/', json={'user': user_id})
        self.assertTrue(resp.status_code == 201)
        
    def testUseValid(self):
        orig = requests.get('http://hasan.d8u.us:5001').json()
        logging.debug("Currently: {0}".format(orig['response']))
        requests.put('http://hasan.d8u.us:5001/0/{0}'.format(orig['response'][0][1]))
        after = requests.get('http://hasan.d8u.us:5001').json()['response']
        logging.debug('After: {0}'.format(after))
        self.assertTrue(len(after) < len(orig['response']))

    def testUseInvalid(self):
        orig = requests.get('http://hasan.d8u.us:5001').json()
        code = orig['response'][0][1]+1
        ret = requests.put('http:/hasan.d8u.us:5001/{0}/{1}'.format(0, code)).json()
        self.assertTrue(ret['error'])

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    unittest.main()

