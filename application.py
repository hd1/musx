import csv
import logging
import random
import string
import os

from flask import Flask, request, jsonify
import requests

app = Flask(__name__)

def LDNOOBW():
    ret = set()
    words = requests.get('https://raw.githubusercontent.com/LDNOOBW/List-of-Dirty-Naughty-Obscene-and-Otherwise-Bad-Words/master/en').content.decode('utf-8')
    [ret.add(w.strip().rstrip()) for w in words.split('\n') if len(w) == 4]
    logging.debug(ret)
    return ret

STOPWORDS = LDNOOBW()

FNAME = 'data.csv'

@app.route("/", methods=['GET'])
def root():
    return jsonify({'response': root_helper()})

def root_helper():
    with open(FNAME) as fin:
        reader = csv.reader(fin)
        next(reader)
        return [r for r in reader]

def get_new_code():
    k = ''
    while k not in STOPWORDS:
        k = ''.join([random.choice(selection) for r in range(1,4)])
    return k

@app.route('/', methods=['POST'])
def new():
    r = request.get_json(force=True, cache=True)
    logging.info('Body is %s', (r))
    with open(FNAME, 'a') as fout:
        writer = csv.writer(fout)
        selection = string.digits + string.ascii_letters
        k = get_new_code()
        writer.writerow([r['user'], k, False])
    return jsonify({'response': [r['user'], k]}), 201

@app.route('/<user_id>/<code>', methods=['PUT'])
def use_as(user_id, code):
    data = root_helper()

    # FIXME use comprehension here
    for idx, row in enumerate(data):
        if code == row[1]:
            row[2] = int(time.time())
            data[idx] = row

    data = [[user, read_code, datetime.datetime.fromtimestamp(0)] for user,read_code in data if code != read_code]

    with open(FNAME, 'w') as fout:
        writer = csv.writer(out)
        writer.writerow(['userid', 'invitation_code', 'used_at'])
        writer.writerows(data)

    return jsonify(codes)

if __name__ == '__main__':
    try:
        with open(FNAME, 'r') as fin: 
            pass
    except IOError:
        logging.error('IOError')
        with open(FNAME, 'w') as fout:
            writer = csv.writer(fout)
            writer.writerow(['userid', 'invitation_code', 'used_at'])

    logging.basicConfig(level=logging.DEBUG)
    app.run(host='0.0.0.0', port=5001, debug=True)
